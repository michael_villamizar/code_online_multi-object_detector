/*

  Online Multi-Object Detection

  Description:
    This file contains the code to perform online learning and detection of
    multiple objects using minimal human supervision [1]. More specifically, this
    code computes multiple online classifiers to learn and detect simultaneously
    various objects using human assistance. The degree of human intervention is 
    reduced progressively in accordance to the classifier performance. Initially, 
    the human selects via the computer's mouse the object in the image that he/she 
    wants to learn and recognize in future frames. Subsequently, each classifier is 
    initially computed using a set of training samples generated artificially. 
    Positive samples are extracted using random shift transformations over the 
    object image, whereas negative samples are random patches from the background.

    In run time, each classifier detects all possible object instances in the image. 
    The classifiers use their own detection predictions and the human assistance to 
    update and refine the object appearance models (interactive learning). Precisely, 
    the human assistance provides the true sample label (class) in those cases where
    the classifier is uncertain abuout its detection predictions (difficult samples).

    If you make use of this code, we kindly encourage to cite the reference [1], 
    listed below. This code is only for research and educational purposes.

  Requirements:
    1. opencv (e.g opencv 2.4.9)
    2. cmake

  Compilation:
    1. mkdir build
    2. cd build
    3. cmake ..
    3. make
    4a. ./detector (for webcam)
    4b. ./detector ../videos/video.avi (for input video file)

  Comments:
    1. The program works at any input image resolution. However, the screen
       information like score, results, etc, are displayed for a resolution
       of 640x480 pixels. This is the default resolution. If you change the
       resolution you must sure that the functions are shown properly. We
       recommend not using a resolution lower of 640x480 pixels.
    2. The program parameters are defined in the file parameters.txt located
       in the files folder. In this file you can change the detector parameters
       and program funcionalities.

  Keyboard commands:
    ESC: exit the program
    space: enable/disable object detection
    v: change the visualization mode (three modes are considered in the program)
    k: change the classifier index to show the learning results on the screen

  Contact:
    Michael Villamizar
    mvillami@iri.upc.edu
    Institut de Robòtica i Informàtica Industrial, CSIC-UPC
    Barcelona - Spain
    2015

  References:
    [1] Modeling Robot's World with Minimal Effort
        M. Villamizar, A. Garrell, A. Sanfeliu and F. Moreno-Noguer
        International Conference on Robotics and Automation (ICRA)
        Seattle, USA, July 2015 

*/


#include <list>
#include <time.h>
#include <string>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <signal.h>
#include "detector_classes.h"
#include "detector_functions.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

using namespace std;

// global variables
CvRect rec = cvRect(-1,-1,0,0);  // bounding box (target)
int visuMode = 1;  // visualization mode
int clfrIndx = 0;  // classifier index (to show learning results)
bool flg_exit = false;  // exit program
bool flg_train = false;  // enable training
bool flg_drawing = false;  // enable drawing circle
bool flg_success = true;  //success flag
bool flg_detection = false;  // enable detection

// functions
void fun_mouse_callback(int event, int x, int y, int flags, void* param);  // mouse callback
void fun_keyboard_callback(clsParameters* prms, cv::Mat &img, char key, int K);  // keyboard callback

// main function
int main( int argc, char* argv[] ){

  // variables
  cv::Mat img;  // image
  time_t start,end;  // times
  char text[50] = "bounding box";  // message
  double dif,fps = 0;  // diff. time and frames per second
  long int contFrame = 0;  // frame counter
  cv::VideoCapture capture;  // video capture
  unsigned short int contFPS = 0;  // frames per second counter

  // program parameters
  clsParameters* prms = new clsParameters;  // initialize parameters
  prms->fun_load();  // load parameters from file
  prms->fun_print();  // print program parameters

  // random ferns
  clsRFs* RFs = new clsRFs;  // initialize random ferns
  RFs->fun_compute(prms->fun_get_pool_size(), prms->fun_get_num_features(), prms->fun_get_fern_size(), prms->fun_get_num_image_channels()); // compute ferns
  RFs->fun_print();  // print random ferns

  // object classifiers
  clsClassifierSet* clfrs = new clsClassifierSet;  // initialize classifiers

  // num. max. classifiers
  int numMaxClfrs = prms->fun_get_num_classifiers();

  // sensitivity learning rate (xi)
  float xi = prms->fun_get_learning_rate();

  // incremental learning rates
  cv::Mat Mc = cv::Mat(1, numMaxClfrs, CV_32FC1, cv::Scalar::all(0.0));  // num. correct predictions
  cv::Mat Mq = cv::Mat(1, numMaxClfrs, CV_32FC1, cv::Scalar::all(0.0));  // num. human assistances

  // uncertainty region thresholds (thetas)
  cv::Mat thetas  = cv::Mat(1, numMaxClfrs, CV_32FC1, cv::Scalar::all(1.0));

  // image size
  int imgWidth = prms->fun_get_image_width();  // width (Iv)
  int imgHeight = prms->fun_get_image_height();  // height (Iu)

  // video capture
  if (argc==1)
    capture.open(0);  // webcam
  if (argc==2)
    capture.open(argv[1]);  // input file

  // check capture
  if (!capture.isOpened()){
    cout << "Error: cannot open the video file" << endl;
    exit(0);
  }

  // window
  cv::namedWindow("detection", CV_WINDOW_AUTOSIZE);

  // initial clock
  time(&start);

  // image stream
  while(1){

    // variables
    cv::Mat detLabels;  // detection labels
    clsDetectionSet* detSet = new clsDetectionSet;  // object detections

    // video frame
    flg_success = capture.read(img); // capture new video frame

    // check capture
    if (!flg_success){
      cout << "Error: cannot read the frame from video file" << endl;
      exit(0);
    }

    // resize input image
    cv::resize(img, img, cvSize(imgWidth, imgHeight));

    // image equalization
    if (prms->fun_image_equalization())
            fun_image_equalization(img);

    // bounding box via mouse callback
    cv::setMouseCallback("detection", fun_mouse_callback, &img);

    // show bounding box
    if (flg_drawing){
      fun_draw_message(prms, img, text, cvPoint(10, imgHeight-100), cvScalar(0, 255, 0));  // print message on screen
      fun_draw_rectangle(prms, img, rec, cvScalar(0, 255, 0));  // draw detection rectangle
    }

    // train step
    if (flg_train){
      flg_train = false;  // reset flag for training
      flg_detection = true;  // set flag for detection
      fun_train_step(prms, clfrs, RFs, img, rec);  // train object classfier
    }

    // detection step
    if (flg_detection)
      fun_test_step(prms, clfrs, RFs, img, detSet);  // test object classifier

    // detection labels
    detLabels = fun_detection_labels(prms, detSet, thetas);

    // updating step
    if (flg_detection)
      fun_update_classifiers(prms, clfrs, RFs, img, detSet, detLabels, Mc, Mq);  // update object classifier

    // draw detections
    fun_draw_detections(prms, clfrs, img, detSet, detLabels);

    // update uncertainty region thresholds (thetas)
    fun_update_uncertainty_thresholds(thetas, Mc, Mq, numMaxClfrs, xi);

    // show frame message
    fun_show_frame(prms, img, contFrame, fps);

    // show learning results
    fun_show_learning_results(prms, img, thetas, Mc, Mq, clfrIndx, contFrame);

    // save image
    if (prms->fun_get_save_images())
      fun_save_image(img, contFrame);

    // update counters
    contFPS++;
    contFrame++;

    // times
    time(&end);
    dif=difftime(end,start);
    if (dif>=1.){
      fps = contFPS/dif;
      printf("Frame count -> %ld : FPS -> %.4f \n", contFrame, fps);
      contFPS = 0;
      time(&start);
    }

    // show image
    cv::imshow("detection", img);
    char key = cv::waitKey(10);

    // keyboard commands
    fun_keyboard_callback(prms, img, key, clfrs->fun_get_num_classifiers());

    // release
    delete detSet;

    // break point
    if (flg_exit) break;
  }

  // release
  delete prms,RFs,clfrs;
  cv::destroyWindow("detection");
  return 0;
}

// mouse callback
void fun_mouse_callback(int event, int x, int y, int flags, void* param ){
  // inputs:
  // event: mouse event
  // x: mouse position in x
  // y: mouse position in y
  // param: image
  
  // input image
  cv::Mat* img = (cv::Mat*) param;

  // events
  switch (event){

    // mouse move
    case CV_EVENT_MOUSEMOVE:{
      if (flg_drawing){
      rec.width  = x - rec.x;
      rec.height = y - rec.y;
      }
    }
    break;

    // left button down
    case CV_EVENT_LBUTTONDOWN:{
      flg_drawing = true;
      rec = cvRect(x, y, 0, 0);
    }
    break;

    // left button up
    case CV_EVENT_LBUTTONUP:{
      flg_drawing = false;
      flg_train = true;
      if (rec.width<0){
        rec.x += rec.width;
        rec.width *= -1;
      }
      if (rec.height<0){
        rec.y += rec.height;
        rec.height *= -1;
      }
    }
    break;

  }
};

// keyboard callback
void fun_keyboard_callback(clsParameters* prms, cv::Mat &img, char key, int K){
  // inputs:
  // prms: program parameters
  // img: image (I)
  // key: keyword key
  // K: num. classifiers

  // keys actions
  switch (key){

    // ESC -> exit
    case 27:
      // message
      cout << endl << "****************************************" << endl;
      cout << "Exit" << endl;
      cout << "****************************************" << endl << endl;
      // exit program
      flg_exit = true;
    break;

    // space -> detection
    case 32:
      if (flg_detection){
        // switch off detection
        flg_detection = false;
        // message
        cout << endl << "****************************************" << endl;
        cout << "Detection [stop]" << endl;
        cout << "****************************************" << endl << endl;
      }
      else{
        // message
        cout << endl << "****************************************" << endl;
        cout << "Detection [start]" << endl;
        cout << "****************************************" << endl << endl;
        // enable detection
        flg_detection = true;
      }
      break;

    // k -> classifier index (k)
    case 107:
      // change object classifier (k)
      clfrIndx = clfrIndx + 1;
      // check
      if (clfrIndx>=K) {clfrIndx = 0;}
      // print message
      cout << endl << "****************************************" << endl;
      cout << "Classifier k: " << clfrIndx << endl;
      cout << "****************************************" << endl << endl;
    break;

    // v -> visualization mode
    case 118:
      // change visualization mode
      visuMode = visuMode + 1;
      // check
      if (visuMode>3) {visuMode=1;}
      // save visualization mode
      prms->fun_set_visualization_mode(visuMode);
      // print message
      cout << endl << "****************************************" << endl;
      cout << "Visualization Mode: " << visuMode << endl;
      cout << "****************************************" << endl << endl;
    break;

  }
};


